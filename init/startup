#!/command/with-contenv sh

# Fresh install
if [ -n "${INSTALL_CREDENTIALS}" ]; then
  echo "WARNING: Install enabled"
  echo ${INSTALL_CREDENTIALS%:*}:$(openssl passwd -apr1 ${INSTALL_CREDENTIALS#*:}) >> /etc/nginx/.htpasswd-users
  sed -ri 's/\#include apps\/wordpress\/wordpress_install\.conf\;/include apps\/wordpress\/wordpress_install\.conf\;/' /etc/nginx/sites-available/www
fi

# Existing git repository
if [ ! -z ${GIT_REPOSITORY} ]; then
  repository=${GIT_REPOSITORY}
  if [ ! -z ${GIT_CREDENTIALS} ]; then
    credentials=${GIT_CREDENTIALS}
    if [ -f /run/secrets/${GIT_CREDENTIALS} ]; then
      credentials=$(cat /run/secrets/${GIT_CREDENTIALS})
    fi
    credentials=$(php -r "\$pos = strpos('${credentials}', ':'); echo substr('${credentials}', 0, \$pos) . ':' . rawurlencode(substr('${credentials}', \$pos + 1));")

    repository=$(echo ${repository} | sed "s|://|://${credentials}@|")
  fi

  if [ -z $(command -v git) ]; then
    apk add --no-cache git
  fi

  git init /srv/www/
  git config --global --add safe.directory /srv/www
  git -C /srv/www/ remote rm origin
  git -C /srv/www/ remote add origin ${repository}
  if [ ! -z ${GIT_TAG} ]; then
    git -C /srv/www/ fetch --depth 1 origin "refs/tags/${GIT_TAG}:refs/tags/${GIT_TAG}"
    git -C /srv/www/ reset --hard tags/${GIT_TAG}
  elif [ ! -z ${GIT_BRANCH} ]; then
    git -C /srv/www/ fetch --depth 1 origin ${GIT_BRANCH}
    git -C /srv/www/ reset --hard origin/${GIT_BRANCH}
  else
    git -C /srv/www/ fetch --depth 1
    git -C /srv/www/ reset --hard origin/master
  fi
  chown -R www-data:www-data /srv/www/
fi

# Run pre-startup scripts
if [ -d /srv/www/init/pre/ ]; then
  run-parts /srv/www/init/pre/
fi

# Database settings
if [ ! -z ${DATABASE} ]; then
  if [ ! -z ${DATABASE_CREDENTIALS} ]; then
    credentials=${DATABASE_CREDENTIALS}
    if [ -f /run/secrets/${DATABASE_CREDENTIALS} ]; then
      credentials=$(cat /run/secrets/${DATABASE_CREDENTIALS})
    fi

    username=${credentials%:*}
    password=${credentials#*:}
  fi

  host=${DATABASE%/*}
  database=${DATABASE#*/}
  cp -f /srv/www/wp-config-sample.php /srv/www/wp-config.php
  chown www-data:www-data /srv/www/wp-config.php
  dos2unix /srv/www/wp-config.php
  sed -i "s/^define.*'DB_NAME'.*$/define( 'DB_NAME', '${database}' );/" /srv/www/wp-config.php
  sed -i "s/^define.*'DB_USER'.*$/define( 'DB_USER', '${username}' );/" /srv/www/wp-config.php
  sed -i "s/^define.*'DB_PASSWORD'.*$/define( 'DB_PASSWORD', '${password}' );/" /srv/www/wp-config.php
  sed -i "s/^define.*'DB_HOST'.*$/define( 'DB_HOST', '${host}' );/" /srv/www/wp-config.php
fi

# Services config
IFS=$'\n'
for variable in $(env); do
  env_name=${variable%%=*}
  value=$(echo ${variable#*=} | sed -e "s/[\/&]/\\\\&/g") # Escape backslash, forwardslash and ampersand

  # WordPress config
  if [ "${env_name:0:17}" = "WORDPRESS_CONFIG_" ]; then
    arg_name=${env_name:17}

    if [ "${value}" != "true" ] || [ "${value}" != "false" ]; then
      value="'${value}'"
    fi

    sed -i "s/^define.*'${arg_name}'.*/define( '${arg_name}', ${value} );/g" /srv/www/wp-config.php
    sed -i "s/^\$${arg_name}.*$/\$${arg_name} = ${value};/g" /srv/www/wp-config.php
  fi

  # Nginx config
  if [ "${env_name:0:19}" = "NGINX_CONFIG_EXTRA_" ]; then
    sed -i "\$i${value}" /etc/nginx/sites-available/www
  elif [ "${env_name:0:13}" = "NGINX_CONFIG_" ]; then
    arg_name=${env_name:13}
    sed -i "s/^\(\s*\)#\?${arg_name}.*$/\1${arg_name} ${value};/" /etc/nginx/nginx.conf
  fi

  # PHP config
  if [ "${env_name:0:11}" = "PHP_CONFIG_" ]; then
    arg_name=${env_name:11}
    sed -i "s/^;\?${arg_name}.*$/${arg_name} = ${value}/" /etc/php7/php.ini
  fi

  # Postfix config
  if [ "${env_name:0:15}" = "POSTFIX_CONFIG_" ]; then
    arg_name=${env_name:15}
    postconf "${arg_name} = ${value}"
  fi
  if [ -n "${POSTFIX_CREDENTIALS}" ]; then
    SIGNATURE=$(echo -n "AWS4${POSTFIX_CREDENTIALS#*:}" | xxd -pc44)
    SIGNATURE=$(echo -n "11111111"       | openssl dgst -binary -sha256 -mac HMAC -macopt "hexkey:${SIGNATURE}" | xxd -pc32)
    SIGNATURE=$(echo -n "${AWS_REGION}"  | openssl dgst -binary -sha256 -mac HMAC -macopt "hexkey:${SIGNATURE}" | xxd -pc32)
    SIGNATURE=$(echo -n "ses"            | openssl dgst -binary -sha256 -mac HMAC -macopt "hexkey:${SIGNATURE}" | xxd -pc32)
    SIGNATURE=$(echo -n "aws4_request"   | openssl dgst -binary -sha256 -mac HMAC -macopt "hexkey:${SIGNATURE}" | xxd -pc32)
    SIGNATURE=$(echo -n "SendRawEmail"   | openssl dgst -binary -sha256 -mac HMAC -macopt "hexkey:${SIGNATURE}" | xxd -pc32)
    echo -n "${POSTFIX_CONFIG_relayhost} ${POSTFIX_CREDENTIALS%:*}:$(echo -n "04${SIGNATURE}" | xxd -rpc32 | base64)" > /etc/postfix/sasl_passwd
    postmap lmdb:/etc/postfix/sasl_passwd
    rm /etc/postfix/sasl_passwd
  fi
done

# Store environment variables
env >> /run/environment

# Run post-startup scripts
if [ -d /srv/www/init/post/ ]; then
  run-parts /srv/www/init/post/
fi
